// 1. В змінній min лежить число (згенерувати рандомне число від 0 до 59) (хвилини)
// Визначити в яку четверть попадає число (першу другу третю четверту)
// Вивести в консоль

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}


const min = getRandomInt(59)


if (min <= 15) {
    console.log('Перша чверть')
} else if (min <= 30 && min >= 16) {
    console.log('Друга чверть')
} else if (min <= 45 && min >= 31) {
    console.log('Третя чверть')
} else {
    console.log('Четверта чверть')
}

console.log(min)

// 2. Змінна lang може приймати два значення ua en . Якщо значення ua тоді в змінну arr присвоїти масив днів тижнів
// українською, якщо значення en тоді англійською.
// Реалізувати через if, switch та/або *багатовимірні масиви
//


let ua = ["пн", "вт", "ср", "чт", "пт", "сб", "нд"]
let en = ["mo", "tu", "we", "th", "fr", "sa", "su"]
let arr = []

// --if--//
let lang = en
if (lang === ua) {
    arr = ua
    console.log(arr)
} else if (lang === en) {
    arr = en
    console.log(arr)
}

//--switch--//
switch (lang) {
    case ua:
        arr = ua
        console.log(arr)
        break
    case en:
        arr = en
        console.log(arr)
        break
    default:
        console.log(`Невірно`)
}

//--багатовимірні масиви--//
let trans = {
    ua: ["пн", "вт", "ср", "чт", "пт", "сб", `нд`],
    en: ["mo", "tu", "we", "th", "fr", "sa", "su"],
}
lang = `ua`
console.log(trans[lang])

//3. В змінній str лежить рядок з цифр, перевірити чи сумма 3-ох перших і 3-ох останніх рівні,
//   якщо так тоді вивести "Щасливий!" інакше "Вам не пощастило".
// доп., для бажаючих згенерувати вхідний рядок з чисел рандомно Math.random()
//

function randomNumber (min, max) {
    min = Math.ceil(100000);
    max = Math.floor(9999999);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
console.log(randomNumber())
let str = randomNumber().toString()
let first = +str[0] + +str[1] + +str[2]
console.log(first)
let last = +str[4] + +str[5] + +str[6]
console.log(last)

if (first === last) {
    console.log(`Щасливий`)
} else {
    console.log(`Вам не пощастило`)
}


